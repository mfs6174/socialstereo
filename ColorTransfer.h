#ifndef COLOR_TRANSFER_H_
#define COLOR_TRANSFER_H_

//#include "global_header.h"
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
using std::vector;
using cv::Mat;


class ColorTranfer
{

public:
	ColorTranfer();
	void solve(const Mat & input, const Mat & target, Mat & output);



private:
	Mat result;
	Mat srcImg;
	Mat targetImg;
	Mat srcImg_lab;//CV_32FC3
	Mat targetImg_lab;//CV_32FC3
	//Mat result_lab;
	vector<float> srcMeans;
	vector<float> srcVariances;
	vector<float> targetMeans;
	vector<float> targetVariances;
	void initialize(const Mat & input, const Mat & target);
	
	void computeMeans();
	
	void computeVariances();
	
};




#endif
