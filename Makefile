include Makefile.common
All :	main.o ColorTransfer.o socialStereo.o
	$(GPP) TEST.bin main.o ColorTransfer.o socialStereo.o
main.o:	ssMain.cpp  common.h socialStereo.h
	$(GPP) main.o -c ssMain.cpp
socialStereo.o:	common.h socialStereo.h ColorTransfer.h socialStereo.cpp
	$(GPP) socialStereo.o -c socialStereo.cpp
ColorTransfer.o:ColorTransfer.cpp ColorTransfer.h
	$(GPP) ColorTransfer.o -c ColorTransfer.cpp
clean:
	rm -rf *.o *.bin

