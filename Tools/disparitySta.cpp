
#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<vector>

#include<opencv2/opencv.hpp>
#include <opencv2/nonfree/features2d.hpp>
using namespace std;
using namespace cv;

void getStat(const Mat &im1,const Mat &im2,Mat &output,vector<Point2f> &stat1,vector<Point2f> &stat2)
{
  SIFT mysift(500);
  Mat g1,g2;
  cvtColor(im1,g1,CV_BGR2GRAY);
  cvtColor(im2,g2,CV_BGR2GRAY);
  vector<KeyPoint> k1,k2;
  Mat d1,d2;
  cout<<"extracting.."<<endl;
  mysift(g1,cv::noArray(),k1,d1);
  mysift(g2,cv::noArray(),k2,d2);
  BFMatcher matcher(NORM_L2,true);
  vector<DMatch> matches;
  cout<<"matching.."<<endl;
  matcher.match(d1,d2,matches);
  for (vector<DMatch>::iterator it=matches.begin();it!=matches.end();)
  {
    if (abs(k1[it->queryIdx].pt.y-k2[it->trainIdx].pt.y)>10)
    {
      it=matches.erase(it);
    }
    else
      it++;
  }
  drawMatches(g1, k1, g2, k2, matches, output, Scalar::all(-1),Scalar::all(-1), vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
  stat1.clear();
  stat2.clear();
  for (int i=0;i<matches.size();i++)
  {
    stat1.push_back(k1[matches[i].queryIdx].pt);
    stat2.push_back(k2[matches[i].trainIdx].pt);
  } 
}
int main(int argc, char *argv[])
{
  if (argc == 3)
  {
    Mat im1=imread(argv[1]),im2=imread(argv[2]);
    Mat output;
    vector<Point2f> stat1,stat2;
    getStat(im1,im2,output,stat1,stat2);
    imwrite("match.png",output);
    for (int i=0;i<stat1.size();i++)
    {
      cout<<"horizon "<<stat1[i].x-stat2[i].x<<"  vertical "<<stat1[i].y-stat2[i].y<<endl;
    }
  }
  else
  {
    if (argc==2)
    {
    }
  }
  return 0;
}
