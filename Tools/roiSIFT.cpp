
#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<vector>

#include<opencv2/opencv.hpp>
#include <opencv2/nonfree/features2d.hpp>
using namespace std;
using namespace cv;

void getStat(const Mat &im1,const Mat &im2,const Mat &m1,const Mat &m2,Mat &output)
{
  SIFT mysift;
  Mat g1,g2;
  cvtColor(im1,g1,CV_BGR2GRAY);
  cvtColor(im2,g2,CV_BGR2GRAY);
  vector<KeyPoint> k1,k2;
  Mat d1,d2;
  cout<<"extracting.."<<endl;
  mysift(g1,m1,k1,d1);
  mysift(g2,m2,k2,d2);
  BFMatcher matcher(NORM_L2,true);
  vector<DMatch> matches,newMatch;
  cout<<"matching.."<<endl;
  matcher.match(d1,d2,matches);
  vector<Point2f> pn1,pn2;
  for (int i=0;i<matches.size();i++)
  {
    pn1.push_back(k1[matches[i].queryIdx].pt);
    pn2.push_back(k2[matches[i].trainIdx].pt);
  }
  vector<uchar> stat;
  Mat F=findFundamentalMat(pn1,pn2,FM_RANSAC,3.0,0.99999,stat);
  cout<<"finish Fundamental"<<endl;
  for (int i=0;i<matches.size();i++)
  {
    if (stat[i]==1)
    {
      newMatch.push_back(matches[i]);
    }
  }
  drawMatches(g1, k1, g2, k2,newMatch, output, Scalar::all(-1),Scalar::all(-1), vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);  
}
int main(int argc, char *argv[])
{
  if (argc == 5)
  {
    Mat im1=imread(argv[1]),im2=imread(argv[2]);
    Mat m1=imread(argv[3],CV_LOAD_IMAGE_GRAYSCALE),m2=imread(argv[4],CV_LOAD_IMAGE_GRAYSCALE);
    cout<<threshold(m1,m1,50,255,THRESH_BINARY| THRESH_OTSU)<<endl;
    cout<<threshold(m2,m2,50,255,THRESH_BINARY| THRESH_OTSU)<<endl;
    imwrite("mask1.png",m1);
    imwrite("mask2.png",m2);
    Mat output;
    getStat(im1,im2,m1,m2,output);
    imwrite("match.png",output);
  }
  else
  {
    if (argc==2)
    {
    }
  }
  return 0;
}
