#include "socialStereo.h"
#include "loadShader.h"
#include "objLoader.h"

using namespace cv;
using namespace std;

int socialStereoWorker::rectifyAllViews()
{
  for (int i=0;i<cams.size();i++)
    for (int j=0;j<cams[i].size();j++)
    {
      cams[i][j].rectify();
    }
  return 0;
}

void ssView::rectify()
{
#ifdef SS_NEED_RECTIFY
  double cof[4]={dr,0,0,0};
  Mat oldInP=inP;
  inP=getOptimalNewCameraMatrix(inP,cof,oriImage.size(),0);
  Mat map1,map2;
  initUndistortRectifyMap(oldInP,cof,Mat(),inP,oriImage.size(),CV_32FC1,map1,map2);
  remap(oriImage,recImage,map1,map2,INTER_LINEAR);
  oriImage.release();//save memory
#else
  recImage=oriImage;
#endif
}

void socialStereoWorker::setModelName(const string &mName)
{
  modelName=mName;
}

int ssGLWorker::init()
{
  width=1280;
  height=720;
  far=1000;
  near=2.0;
  loaded=false;
  if( !glfwInit() )
  {
    fprintf( stderr, "Failed to initialize GLFW\n" );
    return -1;
  }
  glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  
  // Open a window and create its OpenGL context
  window=glfwCreateWindow( width, height, "GL_Woker",0,0);
  if( !window )
  {
    fprintf( stderr, "Failed to open GLFW window\n" );
    glfwTerminate();
    return -1;
  }
  glfwMakeContextCurrent(window);
  printf("the glversion is %s \n\n", glGetString( GL_VERSION ));
  
  // Initialize GLEW
  glewExperimental=GL_TRUE; // Needed in core profile
  if (glewInit()!=GLEW_OK) {
    fprintf(stderr, "Failed to initialize GLEW\n");
    return -1;
  }

  glShadeModel(GL_SMOOTH);						// 启用阴影平滑
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);					// 黑色背景
  glClearDepth(1.0f);							// 设置深度缓存
  glEnable(GL_DEPTH_TEST);						// 启用深度测试
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glDepthFunc(GL_LESS);							// 所作深度测试的类型
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);			// 告诉系统对透视进行修正
  glViewport(0, 0, width, height);

  //glfwHideWindow(window); //we may hide the window
  return 0;
}

int socialStereoWorker::getAllDepthMap()
{
  int ret;
  ret=glWorker.init();
  if (ret<0)
    return ret;
  glWorker.loadModel(modelName);
  for (int i=0;i<cams.size();i++)
    for (int j=0;j<cams[i].size();j++)
    {
      glWorker.setMVP(cams[i].height,cams[i].width,cams[i][j].inP,cams[i][j].exR,cams[i][j].exT);
      glWorker.harvestMatrix(cams[i][j].Mod,cams[i][j].Proj);
      glWorker.render();
      cout<<"render with depth "<<i<<' '<<j<<endl;
      //waitKey(1000);
      glWorker.harvestDepth(cams[i][j].depth,cams[i][j].selfMask);
    }
  return 0;
}

void ssGLWorker::convertToPoint(vector<Point3f> &vv)
{
  vv.clear();
  vv.reserve(vertices.size());
  for (int i=0;i<vertices.size();i++)
  {
    vv.push_back(Point3f(vertices[i].x,vertices[i].y,vertices[i].z));
  }
}

void  socialStereoWorker::getAllMapMesh()
{
  vector<Point3f> mpoint;
  vector<Point2f> ipoint;
  glWorker.convertToPoint(mpoint);
  for (int i=0;i<cams.size();i++)
    for (int j=0;j<cams[i].size();j++)
    {
      Mat rv;
      Rodrigues(cams[i][j].exR,rv);
      projectPoints(mpoint,rv,cams[i][j].exT,cams[i][j].inP,Mat(),ipoint);
      //cout<<cams[i][j].exR<<endl<<cams[i][j].exT<<endl<<cams[i][j].inP<<endl<<endl;
      Mat canv;
      cams[i][j].recImage.copyTo(canv);
      Point tri[3];
      for (int o=0;o<ipoint.size();o+=3)
      {
        bool draw=true;
        for (int p=0;p<3;p++)
        {
          tri[p].x=cvRound(ipoint[o+p].x);
          tri[p].y=cvRound(ipoint[o+p].y);
          if (tri[p].x<0 || tri[p].y < 0 || tri[p].x >= cams[i][j].recImage.cols || tri[p].y >= cams[i][j].recImage.rows )
          {
            draw=false;
            break;
          }
        }
        if (!draw)
          continue;
        line(canv,tri[0],tri[1],Scalar(0,255,0));
        line(canv,tri[0],tri[2],Scalar(0,255,0));
        line(canv,tri[2],tri[1],Scalar(0,255,0));
      }
      imwrite("M_"+str(i)+"_"+str(j)+".jpg",canv);
    }
}


int ssGLWorker::loadModel(const string &mName)
{
  glGenVertexArrays(1, &VertexArrayID);
  glBindVertexArray(VertexArrayID);
  programID = LoadShaders("ssVS.vert","ssFS.frag");
  glUseProgram(programID);
  bool res = loadOBJ(mName.c_str(), vertices, uvs, normals);
  glGenBuffers(1, &vertexbuffer);
  glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

  		// 1rst attribute buffer : vertices
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
  glVertexAttribPointer(
                        0,                  // attribute
                        3,                  // size
                        GL_FLOAT,           // type
                        GL_FALSE,           // normalized?
                        0,                  // stride
                        (void*)0            // array buffer offset
                        );
  loaded=true;
  return res;
}

int ssGLWorker::setMVP(int _height,int _width,const cv::Mat &ip,const cv::Mat &er,const cv::Mat &et)
{
  width=_width;
  height=_height;
  glViewport(0, 0, width, height);
  Mat expandedR;
  er.copyTo(expandedR);

  // Mat C=expandedR.inv()*(-et);
  // //negate the third column of R:
  // for (int i=0;i<3;i++)
  //   expandedR.at<float>(i,2)=-expandedR.at<float>(i,2);
  // if (determinant(expandedR)<0)
  // {
  //   expandedR=-expandedR;
  // }
  // Mat net=-expandedR*C;

  
  Mat Rt = Mat::zeros(4, 4, CV_32FC1);
  for (int y = 0; y < 3; y++) {
    for (int x = 0; x < 3; x++) {
      Rt.at<float>(y, x) = expandedR.at<float>(y, x);
    }
    Rt.at<float>(y, 3) = et.at<float>(y, 0);
  }
  Rt.at<float>(3, 3) = 1.0;

  
  float A=far+near,B=far*near;
  float F=ip.at<float>(0,0);
  float cx=ip.at<float>(0,2),cy=ip.at<float>(1,2);
  float last=1;

  // //negate the third column of K:
  // cx=-ip.at<float>(0,2);
  // cy=-ip.at<float>(1,2);
  // last=-1;
  
  Mat projMat = Mat::zeros(4, 4, CV_32FC1);
  projMat.at<float>(0, 0) = F;
  projMat.at<float>(0, 2) = cx;
  projMat.at<float>(1, 1) = F;
  projMat.at<float>(1, 2) = cy;
  projMat.at<float>(2, 2) = -A;
  projMat.at<float>(2, 3) = B;
  projMat.at<float>(3, 2) = last;
  
  Mat mvMat =Rt;

  for (int i=0;i<4;i++)
    for (int j=0;j<4;j++)
    {
      Model[i][j]=mvMat.at<float>(j,i);
      Projection[i][j]=projMat.at<float>(j,i);
    }


  glm::mat4 Orth=glm::ortho(0.0f,(float)width,(float)height,0.0f,(float)near,(float)far);
  Projection=Orth*Projection;

  MVP=Projection*Model;
  return 0;
}

int ssGLWorker::harvestMatrix(GLdouble _m[16],GLdouble _p[16])
{
  for (int i=0;i<4;i++)
    for (int j=0;j<4;j++)
    {
      _m[j*4+i]=(GLdouble)Model[j][i];
      _p[j*4+i]=(GLdouble)Projection[j][i];
    }
  return 0;
}

int ssGLWorker::render()
{
  if (!loaded)
  {
    cout<<"model not loaded"<<endl;
    return -1;
  }
  glfwSwapBuffers(window);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);			// 清除屏幕和深度缓存
  GLuint MatrixID = glGetUniformLocation(programID, "MVP");
  glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
  //draw the model here
  glDrawArrays(GL_TRIANGLES, 0, vertices.size() );
  return 0;
}

int ssGLWorker::harvestDepth(cv::Mat &_dep,cv::Mat &_mask)
{
  _dep.create(height,width,CV_32FC1);
  _mask.create(height,width,CV_8UC1);
  //use fast 4-byte alignment (default anyway) if possible
  glPixelStorei(GL_PACK_ALIGNMENT, (_dep.step & 3) ? 1 : 4);
  //set length of one complete row in destination data (doesn't need to equal img.cols)
  glPixelStorei(GL_PACK_ROW_LENGTH, _dep.step/_dep.elemSize());

  glReadPixels(0, 0, _dep.cols, _dep.rows, GL_DEPTH_COMPONENT, GL_FLOAT, _dep.data);
  flip(_dep, _dep, 0);
  for (int i=0;i<height;i++)
  {
    for (int j=0;j<width;j++)
    {
      // cout<<_dep.at<float>(i,j)<<' ';
      //glReadPixels(j, height-i-1, 1,1, GL_DEPTH_COMPONENT, GL_FLOAT, &(_dep.at<float>(i,j)));
      if (_dep.at<float>(i,j)==1.0)
        _mask.at<uchar>(i,j)=0;
      else
        _mask.at<uchar>(i,j)=255;
    }
    //cout<<endl;
  }
  return 0;
}

int socialStereoWorker::getAllReproject()
{
  for (int i=0;i<cams.size();i++)
  {
    cams[i].viewPort[0]=cams[i].viewPort[1]=0;
    cams[i].viewPort[2]=cams[i].width;
    cams[i].viewPort[3]=cams[i].height;
    for (int j=0;j<cams[i].size();j++)
    {
      if (cams[i][j].getReproject(cams[i].viewPort)<0)
        return -1;
    }
  }
  return 0;
}

int ssView::getReproject(GLint _vp[])
{
  p3D.create(recImage.rows,recImage.cols,CV_64FC3);
  for (int i=0;i<recImage.rows;i++)
    for (int j=0;j<recImage.cols;j++)
    {
      if (selfMask.at<uchar>(i,j)==0)
          continue;
      GLdouble winX=j,winY=recImage.rows-i-1,winZ=(GLdouble)depth.at<float>(i,j);
      GLdouble ox,oy,oz;
      GLint ret=gluUnProject(winX,winY,winZ,Mod,Proj,_vp,&ox,&oy,&oz);
      if (ret!=GLU_TRUE)
        return -1;
      p3D.at<Vec3d>(i,j)=Vec3d(ox,oy,oz);
      // cout<<p3D.at<Vec3d>(i,j)<<endl;
    }
  return 0;
}


int ssView::projectTo(const Mat &point3D,const Mat &img,Mat &mask,Mat &result,ColorTranfer &ct) const
{
  int dWidth=mask.cols,dHeight=mask.rows;
  result.create(dHeight,dWidth,CV_8UC3);
  Mat cOutput;
  ct.solve(recImage,img,cOutput);
  //cOutput=recImage;
  GLint vp[4]={0,0,recImage.cols,recImage.rows};
  Mat mapping(dHeight,dWidth,CV_32FC2);
  for (int i=0;i<dHeight;i++)
    for (int j=0;j<dWidth;j++)
    {
      if (mask.at<uchar>(i,j)!=0)
      {
        const Vec3d &pnt=point3D.at<Vec3d>(i,j);
        double winX,winY,winZ;
        GLint ret=gluProject(pnt[0],pnt[1],pnt[2],Mod,Proj,vp,&winX,&winY,&winZ);
        if (ret!=GLU_TRUE)
          return -1;
        winY=recImage.rows-winY-1;
        if (winX>0 && winX<recImage.cols-1 && winY>0 && winY<recImage.rows-1 && winZ>=0 &&winZ<=1)
        {
          mapping.at<Vec2f>(i,j)=Vec2f(winX,winY);
        }
        else
        {
          mapping.at<Vec2f>(i,j)=Vec2f(0,0);
          mask.at<uchar>(i,j)=0;
        }
      }
      else
      {
        mapping.at<Vec2f>(i,j)=Vec2f(0,0);
      }
    }
  remap(recImage,result,mapping,Mat(),INTER_LINEAR);
  
  for (int i=0;i<dHeight;i++)
    for (int j=0;j<dWidth;j++)
      if (mask.at<uchar>(i,j)==0)
        result.at<Vec3b>(i,j)=Vec3b(0,0,0);  
  return 0;
}

int socialStereoWorker::loadAllCameraViews(const string &name)
{
  const string imgPath("visualize/");
  const int maxViewPerCam=5;
  camNum=6;
  for (int i=0;i<camNum;i++)
  {
    cams.push_back(ssCamera());
    for (int j=0;j<=maxViewPerCam;j++)
      cams[i].views.push_back(ssView());
  }
  ifstream inf(name);
  int n;
  inf>>n;
  string iname,oname;
  Mat tmat(3,3,CV_32FC1);
  for (int i=0;i<n;i++)
  {
    int om;
    inf>>iname>>om;
    float fl;
    int px,py;
    float t1,t2,t3,tt;
    inf>>fl;
    inf>>px>>py;
    inf>>t1>>t2>>t3;
    for (int j=0;j<10;j++)
      inf>>tt;
    for (int j=0;j<3;j++)
      for (int k=0;k<3;k++)
        inf>>tmat.at<float>(j,k);
    for (int j=0;j<4;j++)
      inf>>tt;
    int cid=om/10000-1;
    int vid=om%10000;
    if (vid>maxViewPerCam)
      continue;
    cams[cid][vid].oriImage=imread(imgPath+iname+".jpg");
    cams[cid][vid].idx=vid;
    tmat.copyTo(cams[cid][vid].exR);
    cams[cid][vid].exT.create(3,1,CV_32FC1);
    cams[cid][vid].exT.at<float>(0,0)=t1;
    cams[cid][vid].exT.at<float>(1,0)=t2;
    cams[cid][vid].exT.at<float>(2,0)=t3;
    cams[cid][vid].inP=Mat::zeros(3,3,CV_32FC1);
    cams[cid][vid].inP.at<float>(0,0)=cams[cid][vid].inP.at<float>(1,1)=fl;
    cams[cid][vid].inP.at<float>(2,2)=1;
    cams[cid][vid].inP.at<float>(0,2)=px;
    cams[cid][vid].inP.at<float>(1,2)=py;
  }
  for (int j=0;j<camNum;j++)
  {
    cams[j].width=cams[j][0].oriImage.cols;
    cams[j].height=cams[j][0].oriImage.rows;
  }
  return 0;
}

 void socialStereoWorker::prepareData4Rij()
{
  allRij=vector< vector<Mat> >(camNum,vector<Mat>(camNum));
  allDij=vector< vector<Mat> >(camNum,vector<Mat>(camNum));
  allMask=vector< vector<Mat> >(camNum,vector<Mat>(camNum));
  threshMask=vector< vector<Mat> >(camNum,vector<Mat>(camNum));
  andMask=vector<Mat>(camNum);
  orMask=vector<Mat>(camNum);
  fMask=vector<Mat>(camNum);
}

 inline float myDist(const Vec3b &x,const Vec3b &y)
 {
   int xx[3],yy[3];
   for (int i=0;i<3;i++)
   {
     xx[i]=x[i];
     yy[i]=y[i];
   }
   return (float)(abs(xx[0]-yy[0])+abs(xx[1]-yy[1])+abs(xx[2]-yy[2]));
 }
 
 void ssView::diff(const cv::Mat &rij,const cv::Mat &mask,cv::Mat &res,int para_win)
 {
   res.create(recImage.rows,recImage.cols,CV_32FC1);
   for (int i=0;i<recImage.rows;i++)
     for (int j=0;j<recImage.cols;j++)
     {
       if (mask.at<uchar>(i,j)!=0)
       {
         float m1=myDist(recImage.at<Vec3b>(i,j),rij.at<Vec3b>(i,j));
         float m2=m1;
         for (int o=-para_win/2;o<=para_win/2;o++)
           for (int p=-para_win/2;p<=para_win/2;p++)
           {
             int ni=i+o,nj=j+p;
             if (ni<0 || nj<0 || ni>recImage.rows-1 || nj > recImage.cols-1)
               continue;
             if (mask.at<uchar>(ni,nj)==0)
               continue;
             m1=min(m1,myDist(recImage.at<Vec3b>(i,j),rij.at<Vec3b>(ni,nj)));
             m2=min(m2,myDist(rij.at<Vec3b>(i,j),recImage.at<Vec3b>(ni,nj)));
           }
         res.at<float>(i,j)=max(m1,m2);
       }
       else
       {
         res.at<float>(i,j)=0.0f;
       }
     }
 }
 
 void socialStereoWorker::finishWithRij(int id)
 {
   //caculate Dij
   for (int i=0;i<camNum;i++)
     for (int j=0;j<camNum;j++)
     {
       cams[i][id].diff(allRij[i][j],allMask[i][j],allDij[i][j],para_win);
       // allDij[i][j].convertTo(allDij[i][j],CV_32FC1,1.0/765.0);
       normalize(allDij[i][j],allDij[i][j],1,0,NORM_MINMAX);
       threshold(allDij[i][j],threshMask[i][j],para_thresh,1.0,THRESH_BINARY);
       cout<<"finish D "<<i<<' '<<j<<endl;
     }
   //calulate Mask
   for (int k=0;k<camNum;k++)
   {
     andMask[k].create(cams[k].height,cams[k].width,CV_8UC1);
     orMask[k].create(cams[k].height,cams[k].width,CV_8UC1);
     fMask[k].create(cams[k].height,cams[k].width,CV_32FC1);
     for (int i=0;i<cams[k].height;i++)
       for (int j=0;j<cams[k].width;j++)
       {
         bool andV=true,orV=false,obs=false;
         float andF=5000.0f;
         for (int o=0;o<camNum;o++)
         {
           if (o==k)
             continue;
           if (allMask[k][o].at<uchar>(i,j)==0)
             continue;
           andV=andV && (threshMask[k][o].at<float>(i,j)==1.0);
           orV=orV || (threshMask[k][o].at<float>(i,j)==1.0);
           andF=min(andF,allDij[k][o].at<float>(i,j));
           obs=true;
         }
         if (andV&&obs)
           andMask[k].at<uchar>(i,j)=255;
         else
           andMask[k].at<uchar>(i,j)=0;
         if (orV&&obs)
           orMask[k].at<uchar>(i,j)=255;
         else
           orMask[k].at<uchar>(i,j)=0;
         if (obs)
           fMask[k].at<float>(i,j)=andF;
         else
           fMask[k].at<float>(i,j)=0.0f;
       }
     cout<<"finish mask "<<k<<endl;
   }
 }

void socialStereoWorker::setupView(ssView & view,const Mat &P,const Mat &R,const Mat & T,int width,int height)
{
  P.copyTo(view.inP);
  R.copyTo(view.exR);
  T.copyTo(view.exT);
  view.recImage.create(height,width,CV_8UC3);
  glWorker.setMVP(height,width,view.inP,view.exR,view.exT);
  glWorker.harvestMatrix(view.Mod,view.Proj);
  glWorker.render();
  cout<<"render done "<<endl;
  //waitKey(1000);
  glWorker.harvestDepth(view.depth,view.selfMask);
  GLint viewPort[4]={0,0,width,height};
  view.getReproject(viewPort);
}

void socialStereoWorker::stereoFromPairs( const Mat & left, const Mat & right, Mat & out, int offset)
{
	Mat left2 = Mat::zeros(left.size(),CV_8UC3);
	Mat right2 = Mat::zeros(right.size(),CV_8UC3);
	for (int i=0;i<left.cols-offset;i++)
    {
      left.col(i+offset).copyTo(left2.col(i));
      right.col(i).copyTo(right2.col(i+offset));
	}
	vector<Mat> left_layers, right_layers, mix_layers;
	split(left2, left_layers);
	split(right2, right_layers);
	mix_layers.push_back(right_layers[0]);
	mix_layers.push_back(right_layers[1]);
	mix_layers.push_back(left_layers[2]);
	merge(mix_layers,out);
}


void socialStereoWorker::getNewViewPair(const ssView &mview,float offset,Mat &leftView,Mat &rightView,Mat &sOut)
{
  ssView lv,rv;
  Mat offT;
  mview.exT.copyTo(offT);
  offT.at<float>(0,0)=mview.exT.at<float>(0,0)+offset/2.0f;
  setupView(lv,mview.inP,mview.exR,offT,mview.recImage.cols,mview.recImage.rows);
  offT.at<float>(0,0)=mview.exT.at<float>(0,0)-offset/2.0f;
  setupView(rv,mview.inP,mview.exR,offT,mview.recImage.cols,mview.recImage.rows);
  Mat tmask;
  lv.selfMask.copyTo(tmask);
  mview.projectTo(lv.p3D,mview.recImage,tmask,lv.recImage,ct);
  rv.selfMask.copyTo(tmask);
  mview.projectTo(rv.p3D,mview.recImage,tmask,rv.recImage,ct);
  lv.recImage.copyTo(leftView);
  rv.recImage.copyTo(rightView);
  stereoFromPairs(rightView,leftView,sOut,0);

  // ssView rrv;
  // offT.at<float>(0,0)=mview.exT.at<float>(0,0)-offset;
  // setupView(rrv,mview.inP,mview.exR,offT,mview.recImage.cols,mview.recImage.rows);
  // rrv.selfMask.copyTo(tmask);
  // mview.projectTo(rrv.p3D,mview.recImage,tmask,rrv.recImage,ct);
  // stereoFromPairs(rrv.recImage,mview.recImage,sOut,0);
}

void ssGLWorker::getRange(float vRange[3][2])
{
  for (int i=0;i<3;i++)
  {
    vRange[i][0]=maxlongint;
    vRange[i][1]=-maxlongint;
  }
  for (int i=0;i<vertices.size();i++)
  {
    for (int j=0;j<3;j++)
    {
      vRange[j][0]=min(vRange[j][0],vertices[i][j]);
      vRange[j][1]=max(vRange[j][1],vertices[i][j]);
    }
  }
}
  
void socialStereoWorker::initVoxels()
{
  glWorker.getRange(vRange);
  for (int i=0;i<3;i++)
  {
    cout<<vRange[i][0]<<' '<<vRange[i][1]<<endl;
    vRange[i][0]=max(-para_clip,vRange[i][0]);
    vRange[i][1]=min(para_clip,vRange[i][1]);
  }
  for (int i=0;i<3;i++)
  {
    vNum[i]=(vRange[i][1]-vRange[i][0])/para_gridSize+0.5;
  }
  voxels=vector( vector<vector<vox> >(vector<vox>(vNum[2]),vNum[1]),vNum[0]);
}
      
  
