#ifndef SOCIALSTEREO_H
#define SOCIALSTEREO_H

#include "common.h"
#include "ColorTransfer.h"


using std::vector;
using std::string;

class ssGLWorker
{
 public:
  int init();
  int loadModel(const string &mName);
  int setMVP(int _height,int _width,const cv::Mat &ip,const cv::Mat &er,const cv::Mat &et);
  int render();
  int harvestDepth(cv::Mat &_dep,cv::Mat &_mask);
  int harvestMatrix(GLdouble _m[16],GLdouble _p[16]);
  void convertToPoint(vector<cv::Point3f> &vv);
  void getRange(float vRange[3][2]);
  std::vector<glm::vec3> vertices;
 private:
  GLfloat far,near;
  bool loaded;
  int height;
  int width;
  GLuint VertexArrayID;
  GLuint vertexbuffer;
  GLuint colorbuffer;
  GLuint programID;
  GLFWwindow* window;
  std::vector<glm::vec2> uvs;
  std::vector<glm::vec3> normals; // Won't be used at the moment.
  glm::mat4 Projection;
  glm::mat4 Model;
  glm::mat4 MVP;

  GLuint widthLoc,heightLoc,farLoc,nearLoc;
};


struct ssView
{
  int loadView();
  void rectify();
  int getReproject(GLint _vp[]);
  int projectTo(const cv::Mat &point3D,const cv::Mat &img,cv::Mat &mask,cv::Mat &result,ColorTranfer &ct) const;
  void diff(const cv::Mat &rij,const cv::Mat &mask,cv::Mat &res,int para);
  cv::Mat oriImage,recImage;
  //int time;
  int idx;
  cv::Mat exR,inP,exT;
#ifdef SS_NEED_RECTIFY
  double dr;
#endif
  cv::Mat depth,p3D,selfMask;
  GLdouble Mod[16];
  GLdouble Proj[16];
};

struct ssCamera
{
  int loadCamera();
  int size(){return views.size();}
  ssView& operator[](int nIndex){return views[nIndex];}
  int width,height;
  vector<ssView> views;
  GLint viewPort[4];
};

struct vox
{
  bool op;
  bool vld;
};
  
class socialStereoWorker
{
 public:
  socialStereoWorker()
  {
    para_thresh=0.5;
    para_win=9;
    para_gridSize=0.05;
    para_clip=4.0;
  };
  int loadAllCameraViews(const string &name);
  void setModelName(const string &mName);
  int rectifyAllViews();
  int getAllDepthMap();
  void getAllMapMesh();
  int getAllReproject();
  void prepareData4Rij();
  void finishWithRij(int id);
  void setupView(ssView & view,const Mat &P,const Mat &R,const Mat & T,int width,int height);
  void getNewViewPair(const ssView &mview,float offset,Mat &leftView,Mat &rightView,Mat &sOut);
  void stereoFromPairs(const  Mat & left, const Mat & right, Mat & out, int offset);
  ssGLWorker glWorker;
  ColorTranfer ct;
  int camNum;
  vector<ssCamera> cams;
  vector< vector<cv::Mat> >  allRij;
  vector< vector<cv::Mat> >  allDij;
  vector< vector<cv::Mat> > allMask;
  vector< vector<cv::Mat> > threshMask;
  vector< cv::Mat> andMask,orMask,fMask;

  float vRange[3][2];
  int vNum[3];
  vector<vector<vector<vox> > > voxels;
  void initVoxels();
 private:
  string modelName;
  float para_thresh;
  int para_win;
  float para_gridSize;
  float para_clip;
};


inline int val(const string &x)
{
  std::istringstream t(x);
  int r;
  t>>r;
  return r;
}

inline string str(int x)
{
  std::ostringstream t;
  t<<x;
  return t.str();
}
#endif
