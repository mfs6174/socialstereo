#include "common.h"
#include "socialStereo.h"

using namespace std;
using namespace cv;

int main()
{
  socialStereoWorker worker;
  worker.loadAllCameraViews(string("cameras_v2.txt"));
  worker.setModelName(string("d.obj"));
  worker.rectifyAllViews();
  worker.getAllDepthMap();
  //worker.getAllMapMesh();
  worker.getAllReproject();
  worker.prepareData4Rij();
  worker.initVoxels();
  for (int i=0;i<3;i++)
    cout<<worker.vRange[i][0]<<' '<<worker.vRange[i][1]<<' '<<worker.vNum[i]<<endl;
  for (int k=0;k<1;k++)
  {
    for (int i=0;i<worker.camNum;i++)
    {
      // Mat tp;
      // normalize(worker.cams[i][k].depth,tp,0,1,NORM_MINMAX);
      // tp.convertTo(tp,CV_8UC1,255);
      // imwrite("Dp_"+str(k)+"_"+str(i)+".png",tp);
      for (int j=0;j<worker.camNum;j++)
      {
        worker.cams[i][k].selfMask.copyTo(worker.allMask[i][j]);
        worker.cams[j][k].projectTo(worker.cams[i][k].p3D,worker.cams[i][k].recImage,worker.allMask[i][j],worker.allRij[i][j],worker.ct);
        //imwrite("R_"+str(k)+"_"+str(i)+"_"+str(j)+".png",worker.allRij[i][j]);
      }
    }
    worker.finishWithRij(k);
    for (int i=0;i<worker.camNum;i++)
    {
      Mat ttp;
      for (int j=0;j<worker.camNum;j++)
      {
        // worker.allDij[i][j].convertTo(ttp,CV_8UC1,255.0);
        // imwrite("D_"+str(k)+"_"+str(i)+"_"+str(j)+".png",ttp);
        //worker.threshMask[i][j].convertTo(ttp,CV_8UC1,255.0);
        //imwrite("T_"+str(k)+"_"+str(i)+"_"+str(j)+".png",ttp);
      }
      // imwrite("AND_"+str(k)+"_"+str(i)+".png",worker.andMask[i]);
      // imwrite("OR_"+str(k)+"_"+str(i)+".png",worker.orMask[i]);
      worker.fMask[i].convertTo(ttp,CV_8UC1,255.0);
      imwrite("F_"+str(k)+"_"+str(i)+".png",ttp);
      //imwrite("O_"+str(k)+"_"+str(i)+".jpg",worker.cams[i][k].oriImage);
    }
    
    // for (int i=0;i<worker.camNum;i++)
    // {
    //   Mat lv,rv,out;
    //   worker.getNewViewPair(worker.cams[i][k],0.08f,lv,rv,out);
    //   //imwrite("LV_"+str(k)+"_"+str(i)+".png",lv);
    //   //imwrite("RV_"+str(k)+"_"+str(i)+".png",rv);
    //   imwrite("Stereo_"+str(k)+"_"+str(i)+".png",out);
    // }
  }
  return 0;
}










