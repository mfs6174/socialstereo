#version 330 core
#define M_PI 3.1415926535897932384626433832795

layout(location = 0) in vec3 vertexPosition_modelspace;
//layout(location = 1) in vec3 vertexColor;


uniform mat4 MVP;

out vec3 fragmentColor;

void main()
{
  vec4 v = vec4(vertexPosition_modelspace,1); // Transform an homogeneous 4D vector, remember ?


  // Output position of the vertex, in clip space : MVP * position

  gl_Position = MVP * v;
  fragmentColor = vec3(0.5,0.5,0.5);

}
